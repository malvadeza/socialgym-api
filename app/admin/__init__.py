from flask import Blueprint

admin_blueprint = Blueprint('admin_blueprint', __name__, template_folder='templates')

from . import admin
