from flask import render_template, request, flash, redirect, url_for
from . import admin_blueprint
from .. import db
from ..models import Administrador, PersonalTrainer, Aluno

@admin_blueprint.route('/')
def index():
    trainers = PersonalTrainer.query.all()
    return render_template('admin/index.html', trainers=trainers)

@admin_blueprint.route('/alunos', methods=['POST'])
def add_aluno():
    trainer = PersonalTrainer.query.get(int(request.form['trainer']))

    aluno = Aluno(request.form['nome'],
            request.form['email'],
            request.form['senha'],
            trainer=trainer)

    db.session.add(aluno)
    db.session.commit()

    flash(u'Aluno cadastrado com sucesso', 'success')

    return redirect(url_for('admin_blueprint.index'))

@admin_blueprint.route('/trainers', methods=['POST'])
def add_trainer():
    trainer = PersonalTrainer(request.form['nome'],
            request.form['email'],
            request.form['senha'])

    db.session.add(trainer)
    db.session.commit()

    flash(u'Personal Trainer cadastrado com sucesso', 'success')

    return redirect(url_for('admin_blueprint.index'))
