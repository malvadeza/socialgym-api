from flask import Blueprint, jsonify

api = Blueprint('api', __name__)

from . import aluno, personal_trainer, programa, treino, exercicio

@api.app_errorhandler(404)
def page_not_found(e):
    response = jsonify({'error': 'not found'})
    response.status_code = 404

    return response
