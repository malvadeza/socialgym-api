import datetime
from flask import jsonify, url_for, request
from . import api
from .. import db
from ..models import Aluno, Programa, Meta

@api.route('/alunos/<int:id>', methods=['GET'])
def get_aluno(id):
    aluno = Aluno.query.get_or_404(id)

    return jsonify(aluno.to_json())

@api.route('/alunos/<int:id>/metas', methods=['GET'])
def get_metas_aluno(id):
    aluno = Aluno.query.get_or_404(id)

    return jsonify({
        'metas': [meta.to_json() for meta in aluno.metas.order_by(Meta.id.desc())]
    })

@api.route('/alunos/<int:id>/metas', methods=['POST'])
def add_metas_aluno(id):
    content = request.get_json(silent=True, force=True)

    if content:
        aluno = Aluno.query.get_or_404(id)
        meta = Meta(aluno)
        meta.meta = content['meta']

        db.session.add(meta)
        db.session.commit()

        response = jsonify({'metas': [meta.to_json() for meta in aluno.metas]})

        response.status_code = 201

        return response
    else:
        response = jsonify({'error': 'Bad request'})
        response.status_code = 400

        return response

@api.route('/alunos/<int:id>/metas/<int:meta_id>', methods=['PUT'])
def update_meta_aluno(id, meta_id):
    content = request.get_json(silent=True, force=True)

    meta = Meta.query.get_or_404(meta_id)

    if meta.aluno.id == id:
        meta.concluida = True
        meta.data_conclusao = datetime.datetime.strptime(content['data_conclusao'], '%Y-%m-%d').date()

        db.session.add(meta)
        db.session.commit()

        response = jsonify(meta.to_json())
        response.status_code = 200

        return response
    else:
        response = jsonify({'error': 'forbidden'})
        response.status_code = 403

        return response

@api.route('/alunos/<int:id>/metas/<int:meta_id>', methods=['DELETE'])
def delete_meta_aluno(id, meta_id):
    meta = Meta.query.get_or_404(meta_id)

    if meta.aluno.id == id:
        db.session.delete(meta)
        db.session.commit()

        response = jsonify({'msg': 'meta deleted'})
        response.status_code = 200

        return response
    else:
        response = jsonify({'error': 'forbidden'})
        response.status_code = 403

        return response

@api.route('/alunos/<int:id>/seguindo', methods=['GET'])
def get_seguindo_aluno(id):
    aluno = Aluno.query.get_or_404(id)

    return jsonify({
        'seguindo': [{'nome': amigo.nome, 'url': url_for('api.get_aluno', id=amigo.id, _external=True)} for amigo in aluno.seguindo]
    })

@api.route('/alunos/<int:id>/seguidores', methods=['GET'])
def get_seguidores_aluno(id):
    aluno = Aluno.query.get_or_404(id)

    return jsonify({
        'seguidores': [{'nome': amigo.nome, 'url': url_for('api.get_aluno', id=amigo.id, _external=True)} for amigo in aluno.seguidores]
    })

@api.route('/alunos/<int:id>/avaliacoes', methods=['GET'])
def get_avaliacoes_aluno(id):
    aluno = Aluno.query.get_or_404(id)
    
    return jsonify({
        'avaliacoes': [avaliacao.to_json() for avaliacao in aluno.avaliacoes]
    })

@api.route('/alunos/<int:id>/programas', methods=['GET'])
def get_programas_aluno(id):
    aluno = Aluno.query.get_or_404(id)
    
    return jsonify({
        'programas': [programa.to_json() for programa in aluno.programas]
    })

@api.route('/alunos/<int:id>/treinos', methods=['GET'])
def get_treinos_aluno(id):
    aluno = Aluno.query.get_or_404(id)
    programa_ativo = aluno.programas.order_by(Programa.id.desc()).first()

    return jsonify({
        'treinos': [
            {
                'serie': treino.serie,
                'url': url_for('api.get_treino', id=treino.id, _external=True)
            } for treino in programa_ativo.treinos
        ] 
    })


@api.route('/alunos/<int:id>/frequencias', methods=['GET'])
def get_frequencias_aluno(id):
    aluno = Aluno.query.get_or_404(id)
    
    return jsonify({
        'frequencias': [frequencia.to_json() for frequencia in aluno.frequencias]
    })
