from flask import jsonify
from . import api
from ..models import Atividade

@api.route('/atividades/<int:id>', methods=['GET'])
def get_ativiade(id):
    atividade = Atividade.query.get_or_404(id)

    return jsonify(atividade.to_json())
