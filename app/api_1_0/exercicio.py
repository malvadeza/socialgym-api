from flask import jsonify, request
from . import api
from .. import db
from ..models import Exercicio

@api.route('/exercicios', methods=['GET'])
def get_exercicios():
    exercicios = Exercicio.query.all()

    return jsonify({
        'exercicios': [exercicio.to_json() for exercicio in exercicios]
    })

@api.route('/exercicios', methods=['POST'])
def add_exercicio():
    content = request.get_json(silent=True, force=True)

    exercicio = Exercicio(content['nome'], 
            content['tipo'], 
            content['nivel_dificuldade'], 
            content['descricao'])

    db.session.add(exercicio)
    db.session.commit()

    return jsonify({
        'exercicios': [exercicio.to_json() for exercicio in Exercicio.query.all()]
    })


@api.route('/exercicios/<int:id>', methods=['GET'])
def get_exercicio(id):
    exercicio = Exercicio.query.get_or_404(id)

    return jsonify(exercicio.to_json())

@api.route('/exercicios/<int:id>', methods=['DELETE'])
def delete_exercicio(id):
    exercicio = Exercicio.query.get_or_404(id)

    db.session.delete(exercicio)
    db.session.commit()

    response = jsonify({'msg': 'exercicio deleted'})
    response.status_code = 200

    return response

