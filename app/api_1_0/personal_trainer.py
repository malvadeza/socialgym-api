from flask import jsonify
from . import api
from ..models import PersonalTrainer

@api.route('/trainers/<int:id>', methods=['GET'])
def get_trainer(id):
    trainer = PersonalTrainer.query.get_or_404(id)

    return jsonify(trainer.to_json())

@api.route('/trainers/<int:id>/alunos', methods=['GET'])
def get_alunos_trainer(id):
    trainer = PersonalTrainer.query.get_or_404(id)

    return jsonify({
        'alunos': [aluno.to_json() for aluno in trainer.alunos]
    })
