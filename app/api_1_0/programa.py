from flask import jsonify
from . import api
from ..models import Programa

@api.route('/programas/<int:id>', methods=['GET'])
def get_programa(id):
    programa = Programa.query.get_or_404(id)

    return programa.to_json()

def get_treinos_programa(id):
    programa = Programa.query.get_or_404(id)

    return jsonify({
            'treinos': [url_for('api.get_treino', id=treino.id, _external=True) for treino in self.treinos],
    })
