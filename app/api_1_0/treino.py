from flask import jsonify
from . import api
from ..models import Treino

@api.route('/treinos/<int:id>', methods=['GET'])
def get_treino(id):
    treino = Treino.query.get_or_404(id)

    return jsonify(treino.to_json())
