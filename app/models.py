import datetime
from flask import url_for
from . import db

class Administrador(db.Model):
    __tablename__ = 'administrador'

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False, unique=True)
    senha = db.Column(db.String(255), nullable=False)
    
    def __init__(self, nome, email, senha):
        self.nome = nome
        self.email = email
        self.senha = senha

class PersonalTrainer(db.Model):
    __tablename__ = 'personal_trainer'

    id = db.Column(db.Integer, primary_key=True)

    nome = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False, unique=True)
    senha = db.Column(db.String(255), nullable=False)
    data_nascimento = db.Column(db.Date)

    def __init__(self, nome, email, senha, data_nascimento=None):
        self.nome = nome
        self.email = email
        self.senha = senha
        self.data_nascimento = data_nascimento

    def __repr__(self):
        return '<PersonalTrainer: %s - %s>' % (self.nome, self.email)

    def to_json(self):
        return {
                'id': self.id,
                'url': url_for('api.get_trainer', id=self.id, _external=True),
                'nome': self.nome,
                'email': self.email,
                'data_nascimento': str(self.data_nascimento)
        }

seguidores = db.Table('seguidores',
        db.Column('aluno_id', db.Integer, db.ForeignKey('aluno.id')),
        db.Column('amigo_id', db.Integer, db.ForeignKey('aluno.id')),
        db.UniqueConstraint('aluno_id', 'amigo_id')
)

class Aluno(db.Model):
    __tablename__ = 'aluno'

    id = db.Column(db.Integer, primary_key=True)

    nome = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False, unique=True)
    senha = db.Column(db.String(255), nullable=False)
    data_nascimento = db.Column(db.Date)

    trainer = db.relationship("PersonalTrainer", backref=db.backref('alunos', lazy='dynamic', cascade='all, delete-orphan'), lazy='joined')
    trainer_id = db.Column(db.Integer, db.ForeignKey('personal_trainer.id'))

    seguindo = db.relationship("Aluno",
            secondary=seguidores,
            primaryjoin=(seguidores.c.aluno_id == id),
            secondaryjoin=(seguidores.c.amigo_id == id),
            backref=db.backref('seguidores', lazy='dynamic'),
            lazy='dynamic'
    )

    def __init__(self, nome, email, senha, data_nascimento=None, trainer=None):
        self.nome = nome
        self.email = email
        self.senha = senha
        self.data_nascimento = data_nascimento
        self.trainer = trainer

    def __repr__(self):
        return '<Aluno: %s - %s>' % (self.nome, self.email)
    
    def to_json(self):
        json_aluno = {
                'id': self.id,
                'url': url_for('api.get_aluno', id=self.id, _external=True),
                'nome': self.nome,
                'email': self.email,
                'data_nascimento': str(self.data_nascimento),

                'trainer': self.trainer.to_json()
        }

        return json_aluno

class Meta(db.Model):
    __tablename__ = 'meta'
    
    id = db.Column(db.Integer, primary_key=True)

    meta = db.Column(db.Text, nullable=False)

    concluida = db.Column(db.Boolean, default=False)
    data_conclusao = db.Column(db.DateTime)

    aluno = db.relationship("Aluno", backref=db.backref('metas', lazy='dynamic', cascade='all, delete-orphan'), lazy='joined')
    aluno_id = db.Column(db.Integer, db.ForeignKey('aluno.id'))

    def __init__(self, aluno):
        self.aluno = aluno

    def __repr__(self):
        return '<Meta: %s - %s' % (self.meta, self.aluno.nome)

    def to_json(self):
        return {
                'id': self.id,
                'meta': self.meta,
                'concluida': self.concluida,
                'data_conclusao': self.data_conclusao if self.data_conclusao is None else self.data_conclusao.strftime('%Y-%m-%d'),
        }

class Avaliacao(db.Model):
    __tablename__ = 'avaliacao'

    id = db.Column(db.Integer, primary_key=True)

    data_avaliacao = db.Column(db.Date, default=datetime.datetime.utcnow().date)
    peso = db.Column(db.Float)
    altura = db.Column(db.Float)
    fc_repouso = db.Column(db.Integer)
    pa_repouso = db.Column(db.Integer)

    aluno = db.relationship("Aluno", backref=db.backref('avaliacoes', lazy='dynamic', cascade='all, delete-orphan'), lazy='joined')
    aluno_id = db.Column(db.Integer, db.ForeignKey('aluno.id'))

    trainer = db.relationship("PersonalTrainer", backref=db.backref('avaliacoes', lazy='dynamic', cascade='all, delete-orphan'), lazy='joined')
    trainer_id = db.Column(db.Integer, db.ForeignKey('personal_trainer.id'))

    def __init__(self, peso, altura, fc_repouso, pa_repouso, aluno):
        self.peso = peso
        self.altura = altura
        self.fc_repouso = fc_repouso
        self.pa_repouso = pa_repouso

        self.aluno = aluno

    def __repr__(self):
        return '<Avaliacao: %s -> %s - %s>' % (self.aluno, self.trainer, self.data_avaliacao)

    def to_json(self):
        return {
                'id': self.id,
                'data_avaliacao': str(self.data_avaliacao),
                'peso': self.peso,
                'altura': self.altura,
                'fc_repouso': self.fc_repouso,
                'pa_repouso': self.pa_repouso,

                'aluno': self.aluno.to_json(),
                'trainer': self.trainer.to_json()
        }

class Programa(db.Model):
    __tablename__ = 'programa'

    id = db.Column(db.Integer, primary_key=True)

    data_inicio = db.Column(db.Date, default=datetime.datetime.utcnow().date)

    aluno = db.relationship("Aluno", backref=db.backref('programas', lazy='dynamic', cascade='all, delete-orphan'), lazy='joined')
    aluno_id = db.Column(db.Integer, db.ForeignKey('aluno.id'))

    def __init__(self, aluno):
        self.aluno = aluno

    def to_json(self):
        return {
                'id': self.id,
                'data_inicio': str(self.data_inicio),

                'aluno': url_for('api.get_aluno', id=self.aluno_id, _external=True)
        }

class Treino(db.Model):
    __tablename__ = 'treino'

    id = db.Column(db.Integer, primary_key=True)

    serie = db.Column(db.Enum('A', 'B', 'C', 'D', 'E', name='letra_serie'))

    programa = db.relationship("Programa", backref=db.backref('treinos', lazy='dynamic', cascade='all, delete-orphan'), lazy='joined')
    programa_id = db.Column(db.Integer, db.ForeignKey('programa.id'))

    def __init__(self, serie, programa):
        self.serie = serie
        self.programa = programa

    def to_json(self):
        return {
                'id': self.id,
                'serie': self.serie,
                'atividades': [atividade.to_json() for atividade in self.atividades]
        }

class Frequencia(db.Model):
    __tablename__ = 'frequencia'
    __table_args__ = (db.UniqueConstraint('data', 'aluno_id'), )

    id = db.Column(db.Integer, primary_key=True)

    data = db.Column(db.Date, default=datetime.datetime.utcnow().date)

    aluno = db.relationship("Aluno", backref=db.backref('frequencias', lazy='dynamic', cascade='all, delete-orphan'), lazy='joined')
    aluno_id = db.Column(db.Integer, db.ForeignKey('aluno.id'))

    treino = db.relationship("Treino", backref=db.backref('frequencias', lazy='dynamic', cascade='all, delete-orphan'), lazy='joined')
    treino_id = db.Column(db.Integer, db.ForeignKey(Treino.id))

    def __init__(self, treino):
        self.treino = treino

    def to_json(self):
        return {
                'id': self.id,
                'data': str(self.data),

                'aluno': url_for('api.get_aluno', id=self.aluno_id, _external=True)
        }

class Exercicio(db.Model):
    __tablename__ = 'exercicio'

    id = db.Column(db.Integer, primary_key=True)

    nome = db.Column(db.String(255), unique=True)
    tipo = db.Column(db.String(255))
    nivel_dificuldade = db.Column(db.Integer)
    descricao = db.Column(db.Text)

    def __init__(self, nome, tipo, nivel_dificuldade, descricao):
        self.nome = nome
        self.tipo = tipo
        self.nivel_dificuldade = nivel_dificuldade
        self.descricao = descricao

    def __repr__(self):
        return '<Exercicio: %s>' % self.nome

    def to_json(self):
        return {
                'nome': self.nome,
                'tipo': self.tipo,
                'nivel_dificuldade': self.nivel_dificuldade,
                'descricao': self.descricao,
                'url': url_for('api.get_exercicio', id=self.id, _external=True)
        }

class Atividade(db.Model):
    __tablename__ = 'atividade' 
    __table_args__ = (db.UniqueConstraint('exercicio_id', 'treino_id'),)

    id = db.Column(db.Integer, primary_key=True)

    series = db.Column(db.Integer)
    repeticoes = db.Column(db.Integer)
    duracao = db.Column(db.Integer)

    exercicio = db.relationship("Exercicio", lazy='joined')
    exercicio_id = db.Column(db.Integer, db.ForeignKey(Exercicio.id))

    treino = db.relationship("Treino", backref=db.backref('atividades', lazy='dynamic', cascade='all, delete-orphan'), lazy='joined')
    treino_id = db.Column(db.Integer, db.ForeignKey(Treino.id))

    def __init__(self, series, repeticoes, duracao, treino, exercicio):
        self.series = series
        self.repeticoes = repeticoes
        self.duracao = duracao

        self.exercicio = exercicio
        self.treino = treino

    def to_json(self):
        return {
                'series': self.series,
                'repeticoes': self.repeticoes,
                'duracao': self.duracao,

                'exercicio': self.exercicio.to_json()
        }
