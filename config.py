import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.getenv('SECRET_KEY', 'umastringdificildeseadivinhar')

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.getenv('SG_DATABASE_URI') or \
            'sqlite:///%s' % os.path.join(basedir, 'database.sqlite')
    SQLALCHEMY_ECHO = DEBUG

class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
                    'sqlite:///' + os.path.join(basedir, 'data.sqlite')
    DEBUG = False

class HerokuConfig(ProductionConfig):
    SSL_DISABLE = bool(os.environ.get('SSL_DISABLE'))


config = {
        'development': DevelopmentConfig,
        'production': ProductionConfig,
        'heroku': HerokuConfig,

        'default': DevelopmentConfig
}
