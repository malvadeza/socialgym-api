import os
from flask.ext.script import Manager, Shell
from app import create_app, db
from app.models import Administrador, Aluno, Avaliacao, Atividade, PersonalTrainer, Meta, Programa, Exercicio, Frequencia, Treino

app = create_app(os.getenv('FLASK_CONFIG', 'default'))

manager = Manager(app)

def make_shell_context():
    return dict(app=app, 
            db=db, 
            Administrador=Administrador,
            Aluno=Aluno, 
            Avaliacao=Avaliacao,
            Atividade=Atividade,
            PersonalTrainer=PersonalTrainer, 
            Meta=Meta,
            Programa=Programa,
            Exercicio=Exercicio,
            Frequencia=Frequencia,
            Treino=Treino)

manager.add_command('shell', Shell(make_context=make_shell_context))

if __name__ == '__main__':
    manager.run()
